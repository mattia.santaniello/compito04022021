-- es 1
drop database if exists correzioneVerifica;
create database if not exists correzioneVerifica;
use correzioneVerifica;

create table dati(
	cod_ordine varchar(4),
    data_ordine datetime,
    cod_prodotto varchar(3),
    descrizione_prodotto varchar(20),
    prezzo_vendita_prodotto decimal(14,4),
    quantity_prodotto smallint,
    prezzo_unitario_prodotto decimal(14,4),
    giacenza_prodotto smallint,
    cod_cliente varchar(3),
    nominativo varchar(20),
    constraint pk_dati primary key (cod_ordine,cod_prodotto)
);

create table prodotti(
	cod_prodotto varchar(3),
    descrizione_prodotto varchar(20),
    prezzo_unitario_prodotto decimal(14,4),
    giacenza smallint,
    constraint pk_prodotti primary key (cod_prodotto)
);

create table clienti(
	cod_cliente varchar(3),
    nominativo varchar(20),
    constraint cod_cliente primary key (cod_cliente)
);

create table ordini(
	cod_ordine varchar(4),
    data_ordine datetime,
    cod_cliente varchar(3),
    constraint pk_ordini primary key (cod_ordine,cod_cliente),
    constraint fk_cliente_ordini foreign key (cod_cliente) references clienti(cod_cliente)
);


create table descrizioni_ordini(
	cod_ordine varchar(4),
    cod_prodotto varchar(3),
    prezzo_vendita_prodotto decimal(14,4),
    quantity_prodotto smallint,
    constraint fk_prodotto_descrizioni_ordini foreign key (cod_prodotto) references prodotti(cod_prodotto),
    constraint pk_descrizioni_ordini primary key (cod_ordine,cod_prodotto)
);



insert into dati values 
("OR01","2021-01-01 09:00:00","P01","PC xx",1150.00,1,1200.00,9,"C01","Rossi"),
("OR01","2021-01-01 09:00:00","P02","Stampante",300.00,1,300.00,19,"C01","Rossi"),
("OR02","2021-02-02 10:00:00","P01","PC xx",1100.00,2,1200.00,7,"C02","Verdi"),
("OR02","2021-02-02 10:00:00","P03","Tablet YY",350.00,1,350.00,3,"C02","Verdi"),
("OR03","2021-02-02 14:00","P03","Tablet YY",340.00,1,350.00,2,"C01","Rossi"),
("OR04","2021-02-03 11:00","P01","PC xx",1150.00,1,1200.00,5,"C02","Verdi");

insert into clienti (select distinct cod_cliente,nominativo from dati);
insert into prodotti (select cod_prodotto,descrizione_prodotto,prezzo_unitario_prodotto,giacenza_prodotto from dati group by cod_prodotto);
insert into ordini (select cod_ordine,data_ordine,cod_cliente from dati group by cod_ordine);
insert into descrizioni_ordini (select distinct cod_ordine,cod_prodotto,prezzo_vendita_prodotto,quantity_prodotto from dati);

-- es 2
delimiter $$
create trigger aggiornaGiacenza after insert on descrizioni_ordini for each row
begin
	update prodotti set giacenza=giacenza-new.quantity_prodotto where prodotti.cod_prodotto = new.cod_prodotto;
end $$
delimiter ;

-- es 3
select D.cod_ordine,(select sum(quantity_prodotto) from descrizioni_ordini D2 where D2.cod_ordine = D.cod_ordine)*(select sum(prezzo_vendita_prodotto) from descrizioni_ordini D2 where D2.cod_ordine = D.cod_ordine) as importo from descrizioni_ordini D group by cod_ordine;

-- es 4
select cod_cliente,nominativo,
(select sum(prezzo_vendita_prodotto) from 

(select D.cod_ordine,cod_cliente,prezzo_vendita_prodotto 

from descrizioni_ordini D inner join Ordini O on O.cod_ordine=D.cod_ordine) O 

where C.cod_cliente = O.cod_cliente) * 

(select sum(quantity_prodotto) from 

(select D.cod_ordine,D.quantity_prodotto,cod_cliente,prezzo_vendita_prodotto 

from descrizioni_ordini D inner join Ordini O on O.cod_ordine=D.cod_ordine) O 

where C.cod_cliente = O.cod_cliente) 

importo from clienti C group by cod_cliente; 



-- es 5 
select D.cod_prodotto,descrizione_prodotto,(select sum(quantity_prodotto) from descrizioni_ordini D2 where D2.cod_prodotto = D.cod_prodotto)*(select sum(prezzo_vendita_prodotto) from descrizioni_ordini D2 where D2.cod_prodotto = D.cod_prodotto) as importo from descrizioni_ordini D inner join Prodotti P on P.cod_prodotto=D.cod_prodotto group by cod_prodotto;

-- es 6
select cod_prodotto,descrizione_prodotto from prodotti where cod_prodotto = (select cod_prodotto from (select D3.cod_prodotto,(select count(*)*(select sum(quantity_prodotto) from descrizioni_ordini D2 where D2.cod_prodotto = D.cod_prodotto) as frequenza from descrizioni_ordini D where D.cod_prodotto = D3.cod_prodotto) frequenza from descrizioni_ordini D3 group by cod_prodotto order by frequenza desc limit 1) r);

-- es 7
 select nominativo,(select sum(prezzo_vendita_prodotto) from descrizioni_ordini D inner join ordini O on O.cod_ordine=D.cod_ordine where C.cod_cliente = O.cod_cliente) as importo from clienti C;


 -- es 8
select r.data_ordine,
(select count(*) from (select (select cast(data_ordine as date) ) data_ordine from descrizioni_ordini D inner join Ordini O on D.cod_ordine = O.cod_ordine) r2 where r2.data_ordine = r.data_ordine) frequenza
from (select cast(data_ordine as date) data_ordine from descrizioni_ordini D inner join Ordini O on D.cod_ordine = O.cod_ordine)  as r group by data_ordine order by frequenza desc limit 1;
